<?php

namespace Notifier\Configuration;

use Symfony\Component\Config\Definition\Builder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class DiscordConfiguration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new Builder\TreeBuilder('notifier');
        /** @var Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->scalarNode('username')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('title')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
                ->integerNode('color')
                    ->defaultValue(0)
                ->end()
                ->scalarNode('subject')
                    ->defaultNull()
                ->end()
                ->scalarNode('description')
                    ->defaultNull()
                ->end()
                ->scalarNode('url')
                    ->defaultNull()
                ->end()
                ->scalarNode('thumbnail')
                    ->defaultNull()
                ->end()
                ->scalarNode('author')
                    ->defaultNull()
                ->end()
                ->scalarNode('author_icon_url')
                    ->defaultNull()
                ->end()
                ->arrayNode('fields')
                    ->arrayPrototype()
                        ->beforeNormalization()
                            ->ifString()
                            ->then(function ($value) {
                                return ['value' => $value];
                            })
                        ->end()
                        ->children()
                            ->scalarNode('value')
                                ->isRequired()
                                ->cannotBeEmpty()
                            ->end()
                            ->arrayNode('foreach')
                                ->arrayPrototype()
                                    ->variablePrototype()->end()
                                ->end()
                            ->end()
                            ->scalarNode('link')
                                ->defaultNull()
                            ->end()
                            ->booleanNode('inline')
                                ->defaultFalse()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}

<?php

namespace Notifier;

use Notifier\Source\AbstractSource;
use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\ExpressionLanguage;
use Symfony\Component\Mailer\Bridge\Google\Transport\GmailSmtpTransport;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mime\Email;
use Monolog\Logger;
use Notifier\Transporter\TransporterFactory;

class EventHandler
{
    /** @var Mailer */
    private $mailer;

    /** @var Logger */
    private $logger;

    /** @var RewindableGenerator */
    private $sources;

    /** @var array */
    private $transporters = [];

    /** @var TransporterFactory */
    private $transporterFactory;

    /** @var array */
    private $config;

    /** @var ExpressionLanguage */
    private $expressionLanguage;

    public function __construct(
        Mailer $mailer,
        Logger $logger,
        RewindableGenerator $sources,
        RewindableGenerator $transporters,
        TransporterFactory $transporterFactory,
        array $config
    ) {
        $this->mailer = $mailer;
        $this->logger = $logger;
        $this->sources = $sources;
        $this->config = $config;
        $this->transporterFactory = $transporterFactory;
        foreach ($transporters as $name => $transporter) {
            $this->transporters[$name] = $transporter;
        }
        $this->expressionLanguage = new ExpressionLanguage();
        $this->expressionLanguage->register('length', function ($str) {
            return \sprintf('(is_string(%1$s) ? strlen(%1$s) : NULL)', $str);
        }, function ($arguments, $value) {
            if (is_string($value)) {
                return \strlen($value);
            }
            if (\is_iterable($value)) {
                return \count($value);
            }
            if (\is_object($value) && \method_exists($value, 'count')) {
                return $value->count();
            }

            return null;
        });
        $this->expressionLanguage->register('max_length', function ($str, $max) {
            return \sprintf('(is_string(%1$s) ? substr(%1$s, 0, %2$d) : NULL)', $str, $max);
        }, function ($arguments, $value, $max) {
            if (\is_string($value)) {
                return \substr($value, 0, $max);
            }

            return null;
        });
        $this->expressionLanguage->register('last', function ($str, $max) {
            /** @phpstan-ignore-next-line */
            return \sprintf('(is_array(%1$s) ? end(%1$s) : NULL)', $str, $max);
        }, function ($arguments, $value) {
            if (\is_array($value)) {
                return \end($value);
            }

            return null;
        });
        $this->expressionLanguage->register('replace', function ($str, $max) {
            return '';
        }, function ($arguments, $searchOrPattern = null, $replace = null, $subject = null) {
            $subject = (string) $subject;
            if (false !== @preg_match($searchOrPattern, $subject)) {
                return preg_replace($searchOrPattern, $replace, $subject);
            } else {
                return str_replace($searchOrPattern, $replace, $subject);
            }
        });
    }

    public function handle(): int
    {
        $code = 200;

        $this->log(
            'NEW REQUEST',
            [
                '_SERVER' => $_SERVER,
                'headers' => getallheaders(),
                '_GET' => $_GET,
                '_POST' => $_POST,
                'body' => file_get_contents('php://input'),
            ]
        );

        /** @var AbstractSource $source */
        foreach ($this->sources as $source) {
            if ($source->match()
                && ($eventName = $source->getEventName())
                && ($values = $source->getValues())
            ) {
                try {
                    if ($this->doHandle($eventName, $values)) {
                        $code = 201;
                        $this->log(
                            "REQUEST {$eventName} MATCHED",
                            (array) $values
                        );
                    }
                } catch (\Exception $e) {
                    $this->log(
                        "REQUEST {$eventName} FAILED: {$e->getMessage()}",
                        (array) $e->getTrace(),
                        Logger::ERROR
                    );
                    if ($this->config['exception'] ?? false) {
                        $transport = new GmailSmtpTransport(
                            $this->config['exception']['email'],
                            $this->config['exception']['password']
                        );
                        $mailer = new Mailer($transport);
                        $content = sprintf("*** TRACE ***:\n%s\n\n", $e->getTraceAsString());
                        $content .= sprintf("*** SERVER ***:\n%s\n\n", var_export($_SERVER, true));
                        $content .= sprintf("*** HEADERS ***:\n%s\n\n", var_export(getallheaders(), true));
                        $content .= sprintf("*** GET ***:\n%s\n\n", var_export($_GET, true));
                        $content .= sprintf("*** POST ***:\n%s\n\n", var_export($_POST, true));
                        $content .= sprintf("*** BODY ***:\n%s\n\n", file_get_contents('php://input'));
                        $email = (new Email())
                            ->from($this->config['exception']['email'])
                            ->to($this->config['exception']['email'])
                            ->subject('[NOTIFIER] '.$e->getMessage())
                            ->text($content);
                        $mailer->send($email);
                    }

                    return 404;
                }
            }
        }

        return $code;
    }

    private function doHandle(string $eventName, array $requestValues): bool
    {
        if (isset($this->config['events'][$eventName])) {
            foreach ($this->config['events'][$eventName] as $channel => $mapping) {
                $transporter = $this->transporterFactory->build($channel);
                $requestValues['users_mapping'] = $this->config['channels'][$channel]['users_mapping'] ?? [];
                // condition
                if (isset($mapping['condition']) && $mapping['condition']) {
                    $conditionPassed = $this->expressionLanguage->evaluate($mapping['condition'], [
                        'v' => $requestValues,
                    ]);
                    if (!$conditionPassed) {
                        continue;
                    }
                }
                unset($mapping['condition']);
                array_walk_recursive($mapping, function (&$expression, $key) use ($requestValues) {
                    if (is_string($expression)) {
                        try {
                            $expression = $this->expressionLanguage->evaluate($expression, [
                                'v' => $requestValues,
                                'key' => $key,
                            ]);
                        } catch (\Exception $e) {
                            $expression = $expression;
                        }
                    }

                    if (is_string($expression)) {
                        $expression = \preg_replace("/(\\\\n)+/", "\n", $expression);
                    }
                });
                $transporter->send($mapping, $channel);
            }

            return true;
        }

        return false;
    }

    private function log(string $message, array $context = [], int $level = Logger::DEBUG): void
    {
        if (isset($this->config['log_debug']) && $this->config['log_debug']) {
            /** @phpstan-ignore-next-line */
            $this->logger->log($level, $message, $context);
        }
    }
}

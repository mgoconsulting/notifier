<?php

namespace Notifier\Source;

class AtlassianJiraAutomation extends AbstractSource
{
    const USER_AGENT = 'Automation for Jira AC app/1.0';

    public function match(): bool
    {
        return self::USER_AGENT === $this->request->headers->get('user-agent');
    }

    public function getEventName(): ?string
    {
        if ($this->request->headers->get('user-agent')) {
            return 'jiraautomation:'.$this->request->headers->get('event');
        }

        return null;
    }
}

<?php

namespace Notifier\Source;

class AtlassianBitBucket extends AbstractSource
{
    const USER_AGENT = 'Bitbucket-Webhooks/2.0';

    public function match(): bool
    {
        return self::USER_AGENT === $this->request->headers->get('user-agent')
            && $this->request->headers->get('x-event-key');
    }

    public function getEventName(): ?string
    {
        return 'bitbucket:'.$this->request->headers->get('x-event-key');
    }
}

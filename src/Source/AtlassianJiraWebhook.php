<?php

namespace Notifier\Source;

class AtlassianJiraWebhook extends AbstractSource
{
    const USER_AGENT = 'Atlassian Webhook HTTP Client';

    public function match(): bool
    {
        return self::USER_AGENT === $this->request->headers->get('user-agent');
    }

    public function getEventName(): ?string
    {
        $json = @json_decode($this->request->getContent(), true);

        if ($json && ($json['webhookEvent'] ?? false)) {
            return 'jirawebhook:'.str_replace('_', ':', $json['webhookEvent']);
        }

        return null;
    }
}

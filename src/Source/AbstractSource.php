<?php

namespace Notifier\Source;

use Notifier\Util\StrDecoder;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractSource
{
    /** @var Request */
    protected $request;

    abstract public function match(): bool;

    abstract public function getEventName(): ?string;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getValues(): ?array
    {
        $content = $this->request->getContent();
        if ($content && is_array($json = @json_decode($content, true))) {
            array_walk_recursive($json, function (&$value) {
                if ($value && is_string($value)) {
                    $value = StrDecoder::ansi($value);
                }
            });

            return $json;
        }

        return null;
    }
}

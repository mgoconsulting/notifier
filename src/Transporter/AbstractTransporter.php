<?php

namespace Notifier\Transporter;

use Symfony\Component\Notifier\Message\SentMessage;

abstract class AbstractTransporter
{
    const TYPE_CHATTER = 'chatter';

    abstract protected static function doGetType(): string;

    abstract public function send(array $options, string $transport = null): ?SentMessage;

    public static function getType(): string
    {
        $type = static::doGetType();
        if (!in_array($type, [self::TYPE_CHATTER])) {
            throw new \Exception("Transporter type '{$type}' is not handled by the Symfony notifier");
        }

        return $type;
    }
}

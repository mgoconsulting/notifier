<?php

namespace Notifier\Transporter;

class TransporterFactory
{
    /** @var array */
    private $transporters = [];

    /** @var array */
    private $config;

    public function __construct($transporters, $config)
    {
        foreach ($transporters as $name => $transporter) {
            $this->transporters[$name] = $transporter;
        }
        $this->config = $config;
    }

    public function build(string $channel): AbstractTransporter
    {
        $channel = $this->config['channels'][$channel] ?? false;
        if (!($channel['dsn'] ?? false)) {
            throw new \Exception("Channel {$channel} not found");
        }
        $parts = \explode(':', $channel['dsn']);
        if (!isset($parts[0]) || !isset($this->transporters[$parts[0]])) {
            throw new \Exception("Transporter not found for channel {$channel}");
        }

        return $this->transporters[$parts[0]];
    }
}

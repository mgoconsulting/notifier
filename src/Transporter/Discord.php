<?php

namespace Notifier\Transporter;

use Notifier\Configuration\DiscordConfiguration;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\Notifier\Bridge\Discord\DiscordOptions;
use Symfony\Component\Notifier\Bridge\Discord\Embeds\DiscordAuthorEmbedObject;
use Symfony\Component\Notifier\Bridge\Discord\Embeds\DiscordEmbed;
use Symfony\Component\Notifier\Bridge\Discord\Embeds\DiscordFieldEmbedObject;
use Symfony\Component\Notifier\Bridge\Discord\Embeds\DiscordMediaEmbedObject;
use Symfony\Component\Notifier\Chatter;
use Symfony\Component\Notifier\Message\ChatMessage;
use Symfony\Component\Notifier\Message\SentMessage;

class Discord extends AbstractTransporter
{
    /** @var Chatter */
    private $chatter;

    /** @var ExpressionLanguage */
    private $expressionLanguage;

    public function __construct(Chatter $chatter)
    {
        $this->chatter = $chatter;
        $this->expressionLanguage = new ExpressionLanguage();
    }

    protected static function doGetType(): string
    {
        return self::TYPE_CHATTER;
    }

    public function send(array $config, string $transport = null): ?SentMessage
    {
        // validate config
        $config = (new Processor())->processConfiguration(
            new DiscordConfiguration(),
            [$config]
        );

        $chatMessage = new ChatMessage($config['subject'] ?? '');

        $embed = new DiscordEmbed();
        $embed
            ->color($config['color'])
            ->title($config['title']);
        if ($config['url']) {
            $embed->url($config['url']);
        }
        if ($config['description']) {
            $embed->description($config['description']);
        }
        if ($config['thumbnail']) {
            $embed->thumbnail((new DiscordMediaEmbedObject())
                ->url($config['thumbnail']));
        }
        if ($config['author']) {
            $author = new DiscordAuthorEmbedObject();
            $author->name($config['author']);
            if ($config['author_icon_url']) {
                $author->iconUrl($config['author_icon_url']);
            }
            $embed->author($author);
        }
        if (is_array($config['fields']) && $config['fields']) {
            foreach ($config['fields'] as $name => $fieldOption) {
                $field = new DiscordFieldEmbedObject();
                $field->name($name);
                $hasValue = false;
                if (!$fieldOption['foreach'] && strlen($fieldOption['value'])) {
                    $field->value($fieldOption['value']);
                    $hasValue = true;
                } elseif ($fieldOption['foreach']) {
                    if (isset($fieldOption['foreach']) && is_array($fieldOption['foreach'])) {
                        $items = [];
                        foreach ($fieldOption['foreach'] as $key => $values) {
                            try {
                                $items[] = $this->expressionLanguage->evaluate($fieldOption['value'], [
                                    'v' => $values,
                                    'key' => $key,
                                ]);
                            } catch (\Exception $e) {
                                $items[] = $fieldOption['value'];
                            }
                        }
                        $fieldOption['value'] = implode(', ', array_filter($items));
                    }
                    if (isset($fieldOption['link']) && is_string($fieldOption['link'])) {
                        $fieldOption['value'] = sprintf(
                            '[%s](%s)',
                            $fieldOption['value'],
                            $fieldOption['link']
                        );
                    }
                    if (strlen($fieldOption['value'])) {
                        $field->value($fieldOption['value']);
                        $hasValue = true;
                    }
                } else {
                    unset($field);
                }

                if ($hasValue && isset($field)) {
                    $field->inline($fieldOption['inline'] ?? false);
                    $embed->addField($field);
                }
            }
        }

        $discordOptions = (new DiscordOptions())
            ->username($config['username'])
            ->addEmbed($embed);

        $chatMessage->options($discordOptions);
        if ($transport) {
            $chatMessage->transport($transport);
        }

        return $this->chatter->send($chatMessage);
    }
}

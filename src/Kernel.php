<?php

namespace Notifier;

use Notifier\Configuration\Configuration;
use Notifier\Transporter\Discord;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Symfony\Component\Yaml\Yaml;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public function registerBundles(): iterable
    {
        return [
            new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
        ];
    }

    protected function configureContainer(ContainerBuilder $container, LoaderInterface $loader): void
    {
        // check notifier config file is defined
        $notifierConfigPath = dirname(__DIR__).'/notifier-config.yml';
        if (is_readable($notifierConfigPath)) {
            $config = Yaml::parseFile($notifierConfigPath);
        } else {
            echo "Config file '{$notifierConfigPath}' was not found.".PHP_EOL;
            exit(1);
        }

        // validate config
        $config = (new Processor())->processConfiguration(
            new Configuration(),
            [$config]
        );

        // load services
        $loader->load(dirname(__DIR__).'/services.yml');

        // create notifier channels config
        $symfonyNotifierConfig = ['enabled' => true];
        foreach ($config['channels'] as $channel => $channelConfig) {
            $dsn = $channelConfig['dsn'];
            $parsed = parse_url($dsn);
            $scheme = $parsed['scheme'] ?? false;
            if (!$container->has($scheme)) {
                switch ($scheme) {
                    case 'discord':
                        $definition = new Definition();
                        $definition
                            ->setClass(Discord::class)
                            ->setPublic(true)
                            ->setArgument('$chatter', new Reference('chatter'))
                            ->addTag('transporter');
                        break;
                }
                if (!isset($definition)) {
                    echo "Channel '{$scheme}' is not implemented\n";
                    exit(1);
                }
                $container->setDefinition($scheme, $definition);
            } else {
                $definition = $container->getDefinition($scheme);
            }
            $type = call_user_func($definition->getClass().'::getType');
            $symfonyNotifierConfig["{$type}_transports"][$channel] = $dsn;
        }
        $container->loadFromExtension('framework', [
            'notifier' => $symfonyNotifierConfig,
        ]);

        // set parameters
        $container->setParameter('notifier.config', $config);
        /** @var \Symfony\Component\HttpFoundation\Request $request */
        $request = $container->get('request');
        $container->setParameter('host', $request->getSchemeAndHttpHost());
    }
}

<?php

/*
 * php -S localhost:8888 -t ./public/
 */

use Notifier\Source;

require dirname(__DIR__).'/vendor/autoload.php';

define('HOST', 'localhost:8888');

$httpCodeSuccess = 201;
$fixtures = [
    'bitbucket-pipeline-failed' => [
        'headers' => [
            'User-Agent' => Source\AtlassianBitBucket::USER_AGENT,
            'X-Event-Key' => 'repo:commit_status_updated',
        ],
        'http_code' => $httpCodeSuccess,
    ],
    'bitbucket-pipeline-started' => [
        'headers' => [
            'User-Agent' => Source\AtlassianBitBucket::USER_AGENT,
            'X-Event-Key' => 'repo:commit_status_updated',
        ],
        'http_code' => $httpCodeSuccess,
    ],
    'bitbucket-pullrequest-new' => [
        'headers' => [
            'User-Agent' => Source\AtlassianBitBucket::USER_AGENT,
            'X-Event-Key' => 'pullrequest:created',
        ],
        'http_code' => $httpCodeSuccess,
    ],
    'bitbucket-pullrequest-approved' => [
        'headers' => [
            'User-Agent' => Source\AtlassianBitBucket::USER_AGENT,
            'X-Event-Key' => 'pullrequest:approved',
        ],
        'http_code' => $httpCodeSuccess,
    ],
    'bitbucket-pullrequest-comment-created' => [
        'headers' => [
            'User-Agent' => Source\AtlassianBitBucket::USER_AGENT,
            'X-Event-Key' => 'pullrequest:comment_created',
        ],
        'http_code' => $httpCodeSuccess,
    ],
    'bitbucket-pullrequest-merged' => [
        'headers' => [
            'User-Agent' => Source\AtlassianBitBucket::USER_AGENT,
            'X-Event-Key' => 'pullrequest:fulfilled',
        ],
        'http_code' => $httpCodeSuccess,
    ],
    'bitbucket-pullrequest-new' => [
        'headers' => [
            'User-Agent' => Source\AtlassianBitBucket::USER_AGENT,
            'X-Event-Key' => 'pullrequest:created',
        ],
        'http_code' => $httpCodeSuccess,
    ],
    'bitbucket-pullrequest-unapproved' => [
        'headers' => [
            'User-Agent' => Source\AtlassianBitBucket::USER_AGENT,
            'X-Event-Key' => 'pullrequest:unapproved',
        ],
        'http_code' => $httpCodeSuccess,
    ],
    'jiraautomation-ticket-created' => [
        'headers' => [
            'User-Agent' => Source\AtlassianJiraAutomation::USER_AGENT,
            'Event' => 'ticket:created',
        ],
        'http_code' => $httpCodeSuccess,
    ],
    'jiraautomation-comment-created' => [
        'headers' => [
            'User-Agent' => Source\AtlassianJiraAutomation::USER_AGENT,
            'Event' => 'comment:created',
        ],
        'http_code' => $httpCodeSuccess,
    ],
];

foreach ($fixtures as $fixture => $expected) {
    echo "Test {$fixture}\n";
    $filename = __DIR__."/{$fixture}.json";
    if (!file_exists($filename)) {
        exit("ERROR: file {$filename} not found".PHP_EOL);
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, HOST);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $post = file_get_contents(__DIR__."/{$fixture}.json");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

    $headers = [
        'Content-Type: application/json',
        'Content-Length: '.strlen($post),
    ];
    foreach ($expected['headers'] ?? [] as $name => $value) {
        $headers[] = "{$name}: {$value}";
    }

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $content = curl_exec($ch);
    $infos = curl_getinfo($ch);
    if (isset($expected['http_code']) && $expected['http_code'] !== $infos['http_code']) {
        echo "\033[1;31mFAILED Http code {$infos['http_code']}, expecting {$expected['http_code']}.\033[0m\n";
        if ($content) {
            echo "Response: {$content}\n";
        }
        curl_close($ch);
        continue;
    }
    curl_close($ch);
    echo "\033[1;32mOK\033[0m\n";
    sleep(2);
}

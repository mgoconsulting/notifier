<?php

use Notifier\Kernel;
use Symfony\Component\ErrorHandler\Debug;
use Symfony\Component\HttpFoundation\Request;

require dirname(__DIR__).'/vendor/autoload.php';

Debug::enable();
$request = Request::createFromGlobals();
$kernel = new Kernel('dev', true);
$kernel->boot();
$code = $kernel->getContainer()->get('event_handler')->handle($request);
http_response_code($code);
